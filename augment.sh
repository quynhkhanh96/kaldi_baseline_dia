#!/bin/bash
. ./cmd.sh
. ./path.sh
set -e
stage=1
indata=$1
musan_root=$2 
nj=40

if [ "$1" == "" ]; then
  echo "Need an input database ... Run me as: \$me <inputdata>"
else
  mfcc_config=conf/mfcc.conf
  # We need to remove features that are too short after removing silence
  # frames.  We want atleast 1s (500 frames) per utterance.
  minlen=100  
  minnumutts=2

  if [ $stage -le 2 ]; then
    frame_shift=0.01
    # awk -v frame_shift=$frame_shift '{print $1, $2*frame_shift;}' ${indata}/utt2num_frames > ${indata}/reco2dur 
    # Make a version with reverberated speech
    rvb_opts=()
    rvb_opts+=(--rir-set-parameters "0.5, RIRS_NOISES/simulated_rirs/smallroom/rir_list")
    rvb_opts+=(--rir-set-parameters "0.5, RIRS_NOISES/simulated_rirs/mediumroom/rir_list")  
    rvb_opts+=(--rir-set-parameters "0.5, RIRS_NOISES/real_rirs_isotropic_noises/rir_list")

    # Make a reverberated version of the data    
    python steps/data/reverberate_data_dir.py \
      "${rvb_opts[@]}" \
      --speech-rvb-probability 1 \
      --pointsource-noise-addition-probability 0 \
      --isotropic-noise-addition-probability 0 \
      --num-replications 1 \
      --source-sampling-rate 16000 \
      ${indata} ${indata}_reverb || exit 1;

    utils/copy_data_dir.sh --utt-suffix "-reverb" ${indata}_reverb ${indata}_reverb.new
    rm -rf ${indata}_reverb
    mv ${indata}_reverb.new ${indata}_reverb
    ## Add volume perturbing to reverbation data
    utils/data/perturb_data_dir_volume.sh ${indata}_reverb || exit 1;
    ################################################################
    nos_opts=()
    nos_opts+=(--noise-set-parameters "0.5, RIRS_NOISES/pointsource_noises/noise_list")
    nos_opts+=(--noise-set-parameters "0.5, RIRS_NOISES/real_rirs_isotropic_noises/noise_list")    
    # additive noise here.  
    python steps/data/reverberate_data_dir.py \
      "${rvb_opts[@]}" "${nos_opts[@]}"\
      --speech-rvb-probability 1 \
      --pointsource-noise-addition-probability 1 \
      --isotropic-noise-addition-probability 1 \
      --num-replications 1 \
      --source-sampling-rate 16000 \
      ${indata} ${indata}_reverb_rirnoise || exit 1;

    utils/copy_data_dir.sh --utt-suffix "-reverb-rirnoise" ${indata}_reverb_rirnoise ${indata}_reverb.new
    rm -rf ${indata}_reverb_rirnoise
    mv ${indata}_reverb.new ${indata}_reverb_rirnoise    
    # Prepare the MUSAN corpus, which consists of music, speech, and noise
    # suitable for augmentation.

    local/make_musan.sh $musan_root data || exit 1;
    # Get the duration of the MUSAN recordings.  This will be used by the
    # script augment_data_dir.py.
    for name in speech noise music vietnamese; do
      utils/data/get_utt2dur.sh data/musan_${name}
      mv data/musan_${name}/utt2dur data/musan_${name}/reco2dur
    done
    # Augment with musan_music
    python steps/data/augment_data_dir.py --utt-suffix "music" --bg-snrs "18:15:10:8" --num-bg-noises "1" --bg-noise-dir "data/musan_music" ${indata} ${indata}_music || exit 1;
    # Augment with musan_noise
    python steps/data/augment_data_dir.py --utt-suffix "noise" --fg-interval 1 --fg-snrs "17:15:10:8" --fg-noise-dir "data/musan_noise" ${indata} ${indata}_noise || exit 1;
    # Augment with musan_speech
    # python steps/data/augment_data_dir.py --utt-suffix "babble" --bg-snrs "18:15:13:10:8" --num-bg-noises "1:2:3" --bg-noise-dir "data/musan_speech" ${indata} ${indata}_babble || exit 1;
    # Augment with musan_vietnamse
    python steps/data/augment_data_dir.py --utt-suffix "vietnamese" --bg-snrs "18:15:13:10:8" --num-bg-noises "1" --bg-noise-dir "data/musan_vietnamese" ${indata} ${indata}_vietnamese || exit 1;

    # Combine reverb, noise, music, and babble into one directory.
    # utils/data/combine_data.sh "${indata}_aug_6x"  "${indata}_noise" "${indata}_music" "${indata}_babble" "${indata}_vietnamese" "${indata}_reverb" "${indata}_reverb_rirnoise"  || exit 1;
    utils/data/combine_data.sh "${indata}_aug_6x"  "${indata}_noise" "${indata}_music" "${indata}_vietnamese" "${indata}_reverb" "${indata}_reverb_rirnoise"  || exit 1;

    ############### Taking only 1/3 of the augmentation data ${indata}_aug_6x  full_training_database_aug_6x
    numutts=$(wc -l ${indata}_aug_6x/utt2spk | awk '{print $1}')
    numutts=$((numutts/3))  
    utils/subset_data_dir.sh ${indata}_aug_6x $numutts ${indata}_aug_6x_subset_0.3 || exit 1;
    ############### Taking 1/3 of the training data for perturbing volume
    numutts=$(wc -l ${indata}/utt2spk | awk '{print $1}')
    numutts=$((numutts/3))
    utils/subset_data_dir.sh ${indata} $numutts ${indata}_subset_0.3_PerturbedVolume_tmp || exit 1;
    ##### Perturing vulome for 1/3 training data
    utils/data/perturb_data_dir_volume.sh  ${indata}_subset_0.3_PerturbedVolume_tmp || exit 1;
    ./utils/copy_data_dir.sh --utt-suffix "-volumed" ${indata}_subset_0.3_PerturbedVolume_tmp ${indata}_subset_0.3_PerturbedVolume || exit 1;
    rm -r ${indata}_subset_0.3_PerturbedVolume_tmp || exit 1;
    ##### Combine Origin, Augumenation, Pertured volume into one database.
    utils/combine_data.sh "${indata}_augmented"  "${indata}"  "${indata}_aug_6x_subset_0.3" "${indata}_subset_0.3_PerturbedVolume"  || exit 1;
  fi

  ####### Computing MFCC-Hires feature for the augmented database
  if [ $stage -le 3 ]; then
    echo  "Computing MFCC feature for the agumented database"
    au_data=${indata}_augmented
    steps/make_mfcc.sh \
      --mfcc-config ${mfcc_config} \
      --cmd "$train_cmd" \
      --nj $nj ${au_data} || exit 1;
    # steps/compute_cmvn_stats.sh ${au_data} || exit 1;
    utils/fix_data_dir.sh ${au_data} || exit 1;
  fi

fi

