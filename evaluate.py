from pyannote.core import Segment, Annotation
from pyannote.metrics.diarization import DiarizationErrorRate 
import os 
import numpy as np 
import pandas as pd 
import pathlib 
import glob 
import shutil 

DIA_TEST_PATH = '/mnt/4T/a/speaker_diarization/evaluation_data'
RTTM_DIR = os.path.join(DIA_TEST_PATH, 'rttms')
NNET_DIR = 'exp/xvector_nnet_1a'
THRESHOLD = 0.5

def get_annotation(rttm_fpath):

    with open(rttm_fpath, 'r') as rttm_reader:
        content = rttm_reader.readlines()
    rttm = [[elt for elt in l.strip().split(' ') if elt != ''] for l in content]

    segment_labels = [[float(l[3]), float(l[4]), int(l[7])] for l in rttm]

    annotation = Annotation()
    for start_time, duration, spk_lb in segment_labels:
        annotation[Segment(start_time, start_time + duration)] = spk_lb

    return annotation 

def compute_der(truth_path, pred_path):
    ref = get_annotation(truth_path)
    hyp = get_annotation(pred_path)

    metric = DiarizationErrorRate()
    score = metric(ref, hyp)

    return score 

def relabel_speaker_id(old_rttm_path, new_rttm_path):
    with open(old_rttm_path, 'r') as rttm_reader:
        content = rttm_reader.readlines()
    rttm = [[elt for elt in l.strip().split(' ') if elt != ''] for l in content]

    speaker_labels = [int(l[7]) for l in rttm]
    id_ = 0
    unique_ids = []
    relabels = {}
    for label in speaker_labels:
        if label not in unique_ids:
            relabels[label] = id_ 
            id_ += 1
            unique_ids.append(label)

    with open(new_rttm_path, 'a') as rttm_writer:
        for elts in rttm:
            file_id = elts[1]
            start = elts[3]
            dur = elts[4]
            spk_id = relabels[int(elts[7])]
            rs = 'SPEAKER {} 0 {} {} <NA> <NA> {} <NA> <NA>'.format(
                file_id, start, dur, spk_id
            )
            rttm_writer.write(rs + '\n')

def evaluate_der_on_test():

    pathlib.Path('data/test').mkdir(parents=True, exist_ok=True)
    pred_rttm_dir = f'{DIA_TEST_PATH}/pred_rttms'
    pathlib.Path(pred_rttm_dir).mkdir(exist_ok=True)

    avg_DER = 0.
    count = 0

    fname_ders = []
    for test_wav_path in glob.glob(f'{DIA_TEST_PATH}/*/*/*.wav'):
        # os.makedirs('data/test', exist_ok=True)
        # create database for test file 
        os.system(f'python setup_and_make_data.py {test_wav_path}')
        # perform inference 
        os.system(f'./inference.sh')
        # relabel rttm file 
        file_name = test_wav_path.split('/')[-1].split('.')[0]
        pred_rttm_path = os.path.join(pred_rttm_dir, file_name)
        relabel_speaker_id(f'{NNET_DIR}/xvectors_test_segmented_cmn/plda_scores_threshold_{THRESHOLD}/rttm',
                            pred_rttm_path)
        # compute DER score
        truth_rttm_path = os.path.join(RTTM_DIR, file_name)
        der = compute_der(truth_rttm_path, pred_rttm_path)
        print(f'DER: {der:.3f}\n******************************************************************************')
        
        fname_ders.append([file_name, der])
        
        # remove temp files 
        os.system(f'rm -r {NNET_DIR}/xvectors_test_segmented_cmn')
        os.system(f'rm -r exp/make_mfcc')
        os.system(f'rm -r exp/make_vad')
        os.system(f'rm -r exp/test_segmented_cmn')

        for subdir in os.listdir('data'):
            os.system(f'rm -r data/{subdir}')

        avg_DER += der 
        count += 1 

    der_df = pd.DataFrame(fname_ders, columns=['fname', 'der'])
    der_df.to_csv('der.csv', index=False)

    avg_DER /= count 

    print(f'==== average DER: {avg_DER}')

if __name__ == '__main__':
    
    evaluate_der_on_test()