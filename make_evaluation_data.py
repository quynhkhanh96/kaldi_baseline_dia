'''
    RTTM format: <type> <file> <chnl> <tbeg> <tdur> <ortho> <stype> <name> <conf> <slat>
    where:
        <type> = "SPEAKER"
        <file> = <recording-id>
        <chnl> = "0"
        <tbeg> = start time of segment
        <tdur> = duration of segment
        <ortho> = "<NA>"
        <stype> = "<NA>"
        <name> = <speaker-id>
        <conf> = "<NA>"
        <slat> = "<NA>"
    eg. SPEAKER spk001_test 0   0.000   1.125 <NA> <NA> 5 <NA> <NA>
'''

import os 
import pathlib 
import librosa
import numpy as np
import pandas as pd  
import random
from tqdm import tqdm 
import soundfile as sf
from nonspeech_remove import pyannote_sad 

SAMPLING_RATE = 16000
DATA_PATH = '/home/khanhdtq/Downloads/wav_all/'
DIA_TEST_PATH = '/mnt/4T/a/speaker_diarization/evaluation_data_nonspeech_removed'
os.makedirs(DIA_TEST_PATH, exist_ok=True)
RTTM_DIR = os.path.join(DIA_TEST_PATH, 'rttms')
os.makedirs(RTTM_DIR, exist_ok=True)

def read_audio(fpath, sampling_rate):
    y, _ = librosa.load(fpath, sr=sampling_rate)
    return y 

def create_multispeaker_speech_file(out_id, fpaths, num_speakers, same_gender=True):

    spkr_dict = {}
    spkr_ids = [fpath.split('/')[-1].split('-')[0] for fpath in fpaths]
    unique_ids = []
    id_ = 0
    for spkr_id in spkr_ids:
        if spkr_id not in unique_ids:
            spkr_dict[spkr_id] = id_ 
            id_ += 1 
            unique_ids.append(spkr_id) 
    print(spkr_dict)
    multispeaker_speech = []
    rttm_lines = []

    print(f'[HEY] {out_id} =====================')
    offset = 0.0 
    for fpath in fpaths:
        speech = read_audio(fpath, SAMPLING_RATE) 
        # dur = len(speech) / SAMPLING_RATE
        dur = librosa.get_duration(speech, SAMPLING_RATE)
        print('--this fpath dur: ', dur)
        # augment data with noise, reverb 
        aug_speech = speech    
        multispeaker_speech.append(aug_speech)     

        this_spkr = spkr_dict[fpath.split('/')[-1].split('-')[0]]
        voiced_segments = pyannote_sad(fpath)
        print(voiced_segments)
        for voiced_segment in voiced_segments:
            print('voiced_segment[0]: ', voiced_segment[0])
            start = voiced_segment[0] + offset 
            print('start: ', start)
            dur = voiced_segment[1] - voiced_segment[0]
            this_rttm = f'SPEAKER {out_id} 0 {start:.3f} {dur:.3f} <NA> <NA> {this_spkr} <NA> <NA>'
            rttm_lines.append(this_rttm)
        
        offset += dur 

    multispeaker_speech = np.hstack(multispeaker_speech)

    if num_speakers < 7:
        num_spkr = '3to6'
    else:
        num_spkr = '7to10'

    if same_gender:
        gender = 'same_gender'
    else:
        gender = 'diff_gender'

    # save speech file 
    dir_path = os.path.join(DIA_TEST_PATH, f'{num_spkr}', f'{gender}')
    pathlib.Path(dir_path).mkdir(parents=True, exist_ok=True)
    sf.write(os.path.join(dir_path, f'{out_id}.wav'), multispeaker_speech, SAMPLING_RATE)

    # save rttm file 
    with open(os.path.join(RTTM_DIR, str(out_id)), 'a') as rttm_writer:
        for rttm_line in rttm_lines:
            rttm_writer.write(rttm_line + '\n')

def generate_trials():
    female_spkrs = np.load('speaker_ids/female_speakers.npy')
    male_spkrs = np.load('speaker_ids/male_speakers.npy')

    num_speaker_ranges = [(3, 6), (7, 10)]

    trials = []
    iter_ = 0
    for nspkr_range in num_speaker_ranges:
        print('[INFO] Creating test data for range {} ...'.format(nspkr_range))
        # same gender
        print('[INFO] Creating speech with speakers of same gender (more difficult cases) ...')
        for spkr_set in [female_spkrs, male_spkrs]:
            for _ in range(500):
                num_speaker = random.randint(*nspkr_range)
                fpaths = []

                # chosen_spkrs = np.random.choice(spkr_set, num_speaker)
                chosen_spkrs = np.random.permutation(spkr_set)[: num_speaker]
                for spkr in chosen_spkrs:
                    spkr_files = os.listdir(os.path.join(DATA_PATH, spkr))
                    chosen_fnames = np.random.choice(spkr_files, np.random.choice([1, 2], 1, p=[0.8, 0.2]))
                    chosen_fpaths = [os.path.join(DATA_PATH, spkr, fname) for fname in chosen_fnames] 
                    fpaths += chosen_fpaths 

                np.random.shuffle(fpaths) 

                create_multispeaker_speech_file(iter_, fpaths, num_speaker, 1)

                this_trial = [fpaths, num_speaker, 1] 
                trials.append(this_trial)

                iter_ += 1
                    
        # diff gender 
        print('[INFO] Creating speech with speakers of different gender ...')
        for _ in range(500):
            fpaths = []
            num_speaker = random.randint(*nspkr_range)
            num_males = random.randint(1, num_speaker - 1)
            num_females = num_speaker - num_males 

            # male
            # chosen_male_spkrs = np.random.choice(male_spkrs, num_males)
            chosen_male_spkrs = np.random.permutation(male_spkrs)[: num_males]
            for male_spkr in chosen_male_spkrs:
                male_spkr_files = os.listdir(os.path.join(DATA_PATH, male_spkr))
                chosen_male_fnames = np.random.choice(male_spkr_files, np.random.choice([1, 2], 1, p=[0.8, 0.2]))
                chosen_male_fpaths = [os.path.join(DATA_PATH, male_spkr, fname) for fname in chosen_male_fnames] 
                fpaths += chosen_male_fpaths 

            # female
            # chosen_female_spkrs = np.random.choice(female_spkrs, num_females)
            chosen_female_spkrs = np.random.permutation(female_spkrs)[: num_females]
            for female_spkr in chosen_female_spkrs:
                female_spkr_files = os.listdir(os.path.join(DATA_PATH, female_spkr))
                chosen_female_fnames = np.random.choice(female_spkr_files, np.random.choice([1, 2], 1, p=[0.8, 0.2]))
                chosen_female_fpaths = [os.path.join(DATA_PATH, female_spkr, fname) for fname in chosen_female_fnames] 
                fpaths += chosen_female_fpaths 

            np.random.shuffle(fpaths) 

            create_multispeaker_speech_file(iter_, fpaths, num_speaker, 0)

            this_trial = [fpaths, num_speaker, 0] 
            trials.append(this_trial)

            iter_ += 1 

    df_trials = pd.DataFrame(trials, columns=['fpaths', 'num_speakers', 'same_gender'])

    df_trials.to_csv(os.path.join(DIA_TEST_PATH, 'trials.csv'), index=False)

if __name__ == '__main__':

    generate_trials()

