import numpy as np 
import librosa 

# = Simple energy based VAD ================================================ 
def to_float(_input): # make input to be float64
    if _input.dtype == np.float64:
        return _input, _input.dtype
    elif _input.dtype == np.float32:
        return _input.astype(np.float64), _input.dtype
    elif _input.dtype == np.uint8:
        return (_input - 128) / 128., _input.dtype
    elif _input.dtype == np.int16:
        return _input / 32768., _input.dtype
    elif _input.dtype == np.int32:
        return _input / 2147483648., _input.dtype
    raise ValueError('Unsupported wave file format')


def from_float(_input, dtype): # change input into dtype data type
    if dtype == np.float64:
        return _input, np.float64
    elif dtype == np.float32:
        return _input.astype(np.float32)
    elif dtype == np.uint8:
        return ((_input * 128) + 128).astype(np.uint8)
    elif dtype == np.int16:
        return (_input * 32768).astype(np.int16)
    elif dtype == np.int32:
        print(_input)
        return (_input * 2147483648).astype(np.int32)
    raise ValueError('Unsupported wave file format')

def agc_pe(x,gain_level):    
    # if input power level is below threshold don't  do anything
    # by changing this value u can change the threshold level
    THRESHOLD_VALUE   = -35 # -30 -> -50 
    # set the output power range here
    OUTPUT_POWER_MIN  = -30
    OUTPUT_POWER_MAX  = +30
    # read the number of channel(s) and  size of the input signal    
    N=len(x)    
    # calculate input power
    p_db=10*np.log10(np.sum(x**2)/N);                            
    # check for threshold 
    if np.min(p_db) < THRESHOLD_VALUE:
        #if input below threshold then output = input        
        return x
    
    # check for output power limits
    if (gain_level < OUTPUT_POWER_MIN) or (gain_level > OUTPUT_POWER_MAX):
        #if required output power is out of range then output = input        
        return x

    # calculate the normal value of the output power 
    # as per our gain level
    # gain_level = 10*log10(output_power_Normal)  
    # by solving the equation we get 
    # output_power_Normal=10.^(gain_level/10);
    output_power_Normal = np.power(10,gain_level/10)
    # calculate the energy of the input  (...Refer eq(2))
    # here one more advantage of the energy calculation is to avoid the division
    # in fix point arithmetic division is expensive so try to avoid the division 
    energy = np.sum(x**2)    
    # MONO    
    # calculate the multiplication factor K1 for mono signal (...Refer eq(3))
    K1 = np.sqrt( (output_power_Normal*N) / energy);
    # multiply K1 with input samples to get required output samples      (...Refer eq(4))
    
    y= x*K1            
    return y

def vad_by_energy(wav, win_dur=100): #25ms 

    THRESHOLD = -30
    win_len = int(win_dur * 1e-3 * 16000)
    
    voiced_windows = []
    for start in range(0, len(wav), win_len):
        end = min(start + win_len, len(wav))
        window = wav[start: end]
        window, _ = to_float(window)
        window = agc_pe(window, -15)
        # window power
        p_db = 10 * np.log10(np.sum(window ** 2) / len(window))
        # compare with a threshold
        if np.min(p_db) > THRESHOLD:
            voiced_windows.append((start / 16000, end / 16000))

    voiced_segments = []
    current_voiced_segment = [voiced_windows[0]]
    for i in range(1, len(voiced_windows)):
        # if voiced_windows[i][0] == current_voiced_segment[-1][1]:
        if voiced_windows[i][0] - current_voiced_segment[-1][1] <= win_dur * 1e-3:
            current_voiced_segment.append(voiced_windows[i])
        else: 
            voiced_segments.append((current_voiced_segment[0][0], current_voiced_segment[-1][1]))
            current_voiced_segment = [voiced_windows[i]]

    return voiced_segments

# = PyAnnote pretrained SAD ================================================
import torch
from pyannote.audio.utils.signal import Binarize
# def pyannote_sad(wav_fpath):
#     # speech activity detection model trained on AMI training set
#     sad = torch.hub.load('pyannote/pyannote-audio', 'sad_ami')
#     test_file = {'audio': wav_fpath}
    
#     sad_scores = sad(test_file)
#     binarize = Binarize(offset=0.9, onset=0.9, log_scale=True, 
#                         min_duration_off=0.1, min_duration_on=0.1)

#     # speech regions (as `pyannote.core.Timeline` instance)
#     speech = binarize.apply(sad_scores, dimension=1)

#     voiced_segments = []
#     for segment in speech:
#         voiced_segments.append([segment.start, segment.end])

#     return voiced_segments

# ==========================================================================
# def create_segments_file(file_id, 
#                         wav_fpath, 
#                         segments_fpath, 
#                         remove_nonspeech_func):

#     voiced_segments = remove_nonspeech_func(wav_fpath)

#     new_utt_ids = []
#     with open(segments_fpath, 'a') as f:
#         utt_index = 1 
#         for voiced_segment in voiced_segments:
#             start = voiced_segment[0]
#             end = voiced_segment[1]
#             new_utt_id = f'{file_id}_{utt_index:04}'
#             f.write(f'{new_utt_id} {file_id} {start:.3f} {end:.3f}\n')
#             new_utt_ids.append(new_utt_id)
#             utt_index += 1 

#     return new_utt_ids 

def pyannote_sad(wav_fpath, sad):
    
    test_file = {'audio': wav_fpath}
    
    sad_scores = sad(test_file)
    binarize = Binarize(offset=0.9, onset=0.9, log_scale=True, 
                        min_duration_off=0.1, min_duration_on=0.1)

    # speech regions (as `pyannote.core.Timeline` instance)
    speech = binarize.apply(sad_scores, dimension=1)

    voiced_segments = []
    for segment in speech:
        voiced_segments.append([segment.start, segment.end])

    return voiced_segments

SAD = torch.hub.load('pyannote/pyannote-audio', 'sad_ami')
def create_segments_file(file_id, 
                        wav_fpath, 
                        segments_fpath):
        
    voiced_segments = pyannote_sad(wav_fpath, SAD)

    new_utt_ids = []
    with open(segments_fpath, 'a') as f:
        utt_index = 1 
        for voiced_segment in voiced_segments:
            start = voiced_segment[0]
            end = voiced_segment[1]
            new_utt_id = f'{file_id}_{utt_index:04}'
            f.write(f'{new_utt_id} {file_id} {start:.3f} {end:.3f}\n')
            new_utt_ids.append(new_utt_id)
            utt_index += 1 

    return new_utt_ids 
    
# if __name__ == '__main__':

#     test_fpath = '/mnt/4T/a/speaker_diarization/evaluation_data/3to6/same_gender/454.wav'
#     # wav, _ = librosa.load(test_fpath, sr=16000)
#     # voiced_segments = vad_by_energy(wav)
#     # for voiced_segment in voiced_segments:
#     #     print(voiced_segment)

#     # wav_dur = len(wav) / 16000
#     # print('total dur: ', wav_dur)
#     # voiced_dur = sum([timestamp[1] - timestamp[0] for timestamp in voiced_segments])
#     # print('voiced dur: ', voiced_dur)

#     create_segments_file(
#         # utt_id='spk001_test_0001',
#         file_id='spk001_test',
#         wav_fpath=test_fpath,
#         segments_fpath='./segments',
#         remove_nonspeech_func=pyannote_sad
#     )


