import os 
import librosa 
import sys 
from nonspeech_remove import create_segments_file, pyannote_sad

# input_wav_path = sys.argv[1]
# # rename input file
# # new_wav_path = './data/test/spk001_test.wav'
# os.makedirs('./data/test', exist_ok=True)
# utt_id = 'spk001_test_0001'
# spk_id = 'spk001'
# file_id = 'spk001_test'
# # wav.scp
# with open('./data/test/wav.scp', 'a') as f:
# #    f.write(f'{file_id} sox -t wav {input_wav_path} -c 1 -b 16 -r 16000 -t wav {new_wav_path}')
#     f.write(f'{file_id} sox {input_wav_path} -c 1 -t wav -r 16000 -b 16 - |')
# #    f.write(f'{file_id} {input_wav_path}\n')     
# # utt2spk
# with open('./data/test/utt2spk', 'a') as f:
#     f.write(f'{utt_id} {spk_id}\n')
# # spk2utt
# with open('./data/test/spk2utt', 'a') as f:
#     f.write('{utt_id} {spk_id}\n')

# # text 
# with open('./data/test/text', 'a') as f:
#     f.write(f'{utt_id} xin chao\n')

# # segments
# # ## naive 
# # y, _ = librosa.load(input_wav_path, sr=16000)
# # duration = librosa.get_duration(y, sr=16000)

# # start = 0.
# # with open('./data/test/segments', 'a') as f:
# #     f.write(f'{utt_id} {file_id} {start:.3f} {duration:.3f}\n')
# create_segments_file(
#     utt_id=utt_id,
#     file_id=file_id,
#     wav_fpath=input_wav_path,
#     segments_fpath='./data/test/segments',
#     remove_nonspeech_func=pyannote_sad
# )

###############################################################
input_wav_path = sys.argv[1]
os.makedirs('./data/test', exist_ok=True)

file_id = 'spk001_test'
spk_id = 'spk001'

# segments 
create_segments_file(
    file_id=file_id,
    wav_fpath=input_wav_path,
    segments_fpath='./data/test/segments'
    # remove_nonspeech_func=pyannote_sad
)

# wav.scp
with open('./data/test/wav.scp', 'a') as f:
#    f.write(f'{file_id} sox -t wav {input_wav_path} -c 1 -b 16 -r 16000 -t wav {new_wav_path}')
    f.write(f'{file_id} sox {input_wav_path} -c 1 -t wav -r 16000 -b 16 - |')

with open('./data/test/segments', 'r') as f:
    content = f.readlines()
utt_ids = [l.strip().split(' ')[0] for l in content]
# utt2spk
with open('./data/test/utt2spk', 'a') as f:
    for utt_id in utt_ids:
        f.write(f'{utt_id} {spk_id}\n')

# text 
with open('./data/test/text', 'a') as f:
    for utt_id in utt_ids:
        f.write(f'{utt_id} xin chao\n')
