'''
    Split dataset of 18k speakers into 4 subsets:
    VinBDI1 (7k speakers): train & dev x-vector network
    VinBDI2 (2k speakers): compute mean.vec, transform.mat 
                            to perform PLDA whitening
    VinBDI3 (4k speakers): train PLDA
    VinBDI4 (2k speakers): tune threshold
'''

import os 
import numpy as np 
import pandas as pd 
from nonspeech_remove import pyannote_sad, create_segments_file
from pathlib import Path 
from tqdm import tqdm 

DATABASE_PATH = './database_recording_raw_16k_1channel_cleaned'

# ===========================================================
# speaker id - utterance id 
with open(os.path.join(DATABASE_PATH, 'spk2utt'), 'r') as f:
    content = f.readlines()
spk2utt = [x.strip() for x in content]

vin1_spk2utt = spk2utt[:7000]
vin2_spk2utt = spk2utt[7000:(7000 + 2000)]
vin3_spk2utt = spk2utt[9000: (9000 + 4000)]
vin4_spk2utt = spk2utt[13000: (13000 + 2000)]
vin_18k = {
    'vin1_spk2utt': vin1_spk2utt,
    'vin2_spk2utt': vin2_spk2utt,
    'vin3_spk2utt': vin3_spk2utt,
    'vin4_spk2utt': vin4_spk2utt
    }

# utterance id - wav fpath 
with open(os.path.join(DATABASE_PATH, 'wav.scp'), 'r') as f:
    content = f.readlines()
wavscp = [x.strip().split(' ') for x in content]
df_wavscp = pd.DataFrame(wavscp, columns=['file_id', 'fpath'])

# for each dataset (vinbdi1, 2, 3, 4)
for set_num in range(1, 5):
    os.makedirs(f'data/vinbdi{set_num}', exist_ok=True)
    # new_utt_ids = []
    # for each utt_id from one speaker (or file_ids in wav.scp)
    for l in tqdm(vin_18k[f'vin{set_num}_spk2utt']):
        spk_id = l.split(' ')[0]
        utt_ids = l.split(' ')[1: ]
        # utt_id in spk2utt is file_id in wav.scp 
        for utt_id in utt_ids:
            fpath = df_wavscp[df_wavscp['file_id'] == utt_id]['fpath'].values[0] 
            # if os.path.isfile(fpath) and Path(fpath).stat().st_size > 0:
            # segments    
            new_utt_ids_from_this_file = create_segments_file(
                file_id=utt_id,
                wav_fpath=fpath,
                segments_fpath=f'data/vinbdi{set_num}/segments'
                # remove_nonspeech_func=pyannote_sad
            )
            # new_utt_ids.append(new_utt_ids_from_this_file)
            # new_utt_ids += new_utt_ids_from_this_file 
            # wav.scp 
            with open(f'data/vinbdi{set_num}/wav.scp', 'a') as f:
                f.write(f'{utt_id} {fpath}\n') # file_id - fpath 
            # utt2spk
            with open(f'data/vinbdi{set_num}/utt2spk', 'a') as f:
                for new_utt_id in new_utt_ids_from_this_file:
                    f.write(f'{new_utt_id} {spk_id}\n')
            # text 
            with open(f'data/vinbdi{set_num}/text', 'a') as f:
                for new_utt_id in new_utt_ids_from_this_file:
                    f.write(f'{new_utt_id} xin chao\n')
    