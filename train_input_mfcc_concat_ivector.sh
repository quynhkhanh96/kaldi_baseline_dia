. ./cmd.sh
. ./path.sh
set -e
mfccdir=data/mfcc
vaddir=data/mfcc
# musan_root=/data0/khanhdtq/speaker_diarization/musan
musan_root=/mnt/4T/musan 
nnet_dir=exp/xvector_nnet_1a

oneof=200 # taking only 1/oneof cho UBM and PCA training
gmm_gauss=1024 # Number of Gaussian mixtures for UBM model (512 for small set)

# export CUDA_VISIBLE_DEVICES=5

stage=3

echo '=================== TRAIN X-VECTOR NETWORK ==================='
# Prepare features
echo '[INFO]    Prepare features ...'
if [ $stage -le 0 ]; then
    for name in vinbdi1 vinbdi2 vinbdi3 vinbdi4; do
	echo data/${name} 
        steps/make_mfcc.sh --mfcc-config conf/mfcc.conf --nj 40 --cmd "$train_cmd" --write-utt2num-frames true data/${name} exp/make_mfcc $mfccdir
        utils/fix_data_dir.sh data/${name}

        sid/compute_vad_decision.sh --nj 40 --cmd "$train_cmd" data/${name} exp/make_vad $vaddir
        utils/fix_data_dir.sh data/${name}
    done 
fi 

echo '[INFO]    Apply the sliding window CMN then write feature to disk ...'
# Writes features to disk after applying the sliding window CMN.
if [ $stage -le 1 ]; then
    for name in vinbdi2 vinbdi3 vinbdi4; do 
        local/nnet3/xvector/prepare_feats.sh --nj 40 --cmd "$train_cmd" data/${name} data/${name}_cmn exp/${name}_cmn
        cp data/${name}/vad.scp data/${name}_cmn/       
        if [ -f data/$name/segments ]; then
            cp data/$name/segments data/${name}_cmn/
        fi 
        utils/fix_data_dir.sh data/${name}_cmn
    done 

    echo "0.01" > data/vinbdi3_cmn/frame_shift
    diarization/vad_to_segments.sh --nj 40 --cmd "$train_cmd" data/vinbdi3_cmn data/vinbdi3_cmn_segmented
fi 

echo '[INFO]    Data augmentation for VinBDI1 ...'
if [ $stage -le 2 ]; then
    frame_shift=0.01
    awk -v frame_shift=$frame_shift '{print $1, $2*frame_shift;}' data/vinbdi1/utt2num_frames > data/vinbdi1/reco2dur 

    augment.sh data/vinbdi1 $musan_root # augment then compute MFCC for augmented data
    utils/combine_data.sh data/vinbdi1_combined data/vinbdi1_augmented data/vinbdi1 # combine MFCC of augmented data and original data into ones
fi  

# Now we prepare the features to generate examples for xvector training.
if [ $stage -le 3 ]; then

    # echo '[INFO]        Apply CMN and remove nonspeech frames ...'
    # # This script applies CMN and removes nonspeech frames.  Note that this is somewhat
    # # wasteful, as it roughly doubles the amount of training data on disk.  After
    # # creating training examples, this can be removed.
    # local/nnet3/xvector/prepare_feats_for_egs.sh --nj 40 --cmd "$train_cmd" data/vinbdi1_combined data/vinbdi1_combined_cmn_no_sil exp/vinbdi1_combined_cmn_no_sil
    # utils/fix_data_dir.sh data/vinbdi1_combined_cmn_no_sil

    # echo '[INFO]        Remove features that are too short after removing silence ...'
    # # Now, we need to remove features that are too short after removing silence
    # # frames.  We want atleast 5s (500 frames) per utterance.
    # min_len=500
    # mv data/vinbdi1_combined_cmn_no_sil/utt2num_frames data/vinbdi1_combined_cmn_no_sil/utt2num_frames.bak
    # awk -v min_len=${min_len} '$2 > min_len {print $1, $2}' data/vinbdi1_combined_cmn_no_sil/utt2num_frames.bak > data/vinbdi1_combined_cmn_no_sil/utt2num_frames
    # utils/filter_scp.pl data/vinbdi1_combined_cmn_no_sil/utt2num_frames data/vinbdi1_combined_cmn_no_sil/utt2spk > data/vinbdi1_combined_cmn_no_sil/utt2spk.new
    # mv data/vinbdi1_combined_cmn_no_sil/utt2spk.new data/vinbdi1_combined_cmn_no_sil/utt2spk
    # utils/fix_data_dir.sh data/vinbdi1_combined_cmn_no_sil

    # echo '[INFO]        Filter out speakers with fewer than 8 utterances ...'
    # # We also want several utterances per speaker. Now we'll throw out speakers
    # # with fewer than 8 utterances.
    # min_num_utts=8
    # awk '{print $1, NF-1}' data/vinbdi1_combined_cmn_no_sil/spk2utt > data/vinbdi1_combined_cmn_no_sil/spk2num
    # awk -v min_num_utts=${min_num_utts} '$2 >= min_num_utts {print $1, $2}' data/vinbdi1_combined_cmn_no_sil/spk2num | utils/filter_scp.pl - data/vinbdi1_combined_cmn_no_sil/spk2utt > data/vinbdi1_combined_cmn_no_sil/spk2utt.new
    # mv data/vinbdi1_combined_cmn_no_sil/spk2utt.new data/vinbdi1_combined_cmn_no_sil/spk2utt
    # utils/spk2utt_to_utt2spk.pl data/vinbdi1_combined_cmn_no_sil/spk2utt > data/vinbdi1_combined_cmn_no_sil/utt2spk

    # utils/filter_scp.pl data/vinbdi1_combined_cmn_no_sil/utt2spk data/vinbdi1_combined_cmn_no_sil/utt2num_frames > data/vinbdi1_combined_cmn_no_sil/utt2num_frames.new
    # mv data/vinbdi1_combined_cmn_no_sil/utt2num_frames.new data/vinbdi1_combined_cmn_no_sil/utt2num_frames

    # # Now we're ready to create training examples.q
    # utils/fix_data_dir.sh data/vinbdi1_combined_cmn_no_sil

    echo '[INFO]        Extract i-vector from MFCC of training data (vinbdi1 + augmented vinbdi1) ...'
    workflows/local/nnet3/run_ivector_common_pca_vt.sh expDNN data/vinbdi1_combined_cmn_no_sil ${oneof} ${gmm_gauss} || exit 1;
    # result in: expDNN/ivectors_vinbdi1_combined_cmn_no_sil
fi

# steps from 4 to 6 are handled here 
echo '[INFO]    Train the network ...'
local/nnet3/xvector/tuning/run_xvector_1a.sh --stage $stage --train-stage -1 --data data/vinbdi1_combined_cmn_no_sil --nnet-dir $nnet_dir --egs-dir $nnet_dir/egs expDNN/ivectors_vinbdi1_combined_cmn_no_sil

echo '=================== TRAIN PLDA ==================='
# Extract x-vectors
if [ $stage -le 7 ]; then

    echo '[INFO]    Extract x-vectors for whitening ...'
    echo "0.01" > data/vinbdi2_cmn/frame_shift
    diarization/vad_to_segments.sh --nj 40 --cmd "$train_cmd" data/vinbdi2_cmn data/vinbdi2_cmn_segmented
#    utils/fix_data_dir.sh data/vinbdi2_cmn_segmented
    
#    scp data/vinbdi2_cmn_segmented/segments data/vinbdi2_cmn
#    utils/fix_data_dir.sh data/vinbdi2_cmn 
    diarization/nnet3/xvector/extract_xvectors.sh --cmd "$train_cmd --mem 5G" --nj 40 --window 1.5 --period 0.75 --apply-cmn false --min-segment 0.5 $nnet_dir data/vinbdi2_cmn_segmented $nnet_dir/xvectors_vinbdi2

    # diarization/nnet3/xvector/extract_xvectors.sh --cmd "$train_cmd --mem 5G" --nj 40 --window 1.5 --period 0.75 --apply-cmn false --min-segment 0.5 $nnet_dir data/vinbdi4_cmn $nnet_dir/xvectors_vinbdi4

    # Extract x-vectors for the VinBDI3, which is our PLDA training
    # data.  A long period is used here so that we don't compute too
    # many x-vectors for each recording.
    echo '[INFO]    Extract x-vectors for PLDA traning ...'
    diarization/nnet3/xvector/extract_xvectors.sh --cmd "$train_cmd --mem 10G" --nj 40 --window 3.0 --period 10.0 --min-segment 1.5 --apply-cmn false --hard-min true $nnet_dir data/vinbdi3_cmn_segmented $nnet_dir/xvectors_vinbdi3_cmn_segmented
fi

# train PLDA
if [ $stage -le 8 ]; then
    echo '[INFO]    Train PLDA ...'
    # Train a PLDA model on VinBDI3, using VinBDI2 to whiten.
    # We will later use this to score x-vectors in VinBDI2.
    "$train_cmd" $nnet_dir/xvectors_vinbdi2/log/plda.log ivector-compute-plda ark:$nnet_dir/xvectors_vinbdi3_cmn_segmented/spk2utt "ark:ivector-subtract-global-mean scp:$nnet_dir/xvectors_vinbdi3_cmn_segmented/xvector.scp ark:- | transform-vec $nnet_dir/xvectors_vinbdi2/transform.mat ark:- ark:- | ivector-normalize-length ark:- ark:- |" $nnet_dir/xvectors_vinbdi2/plda || exit 1;
fi
