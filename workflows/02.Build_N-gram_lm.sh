#!/bin/bash
. ./path.sh
. ./cmd.sh
. ./configs.sh
###############
# tmp=resources
# traindata=data/FPTFulldata-NoFRT-20190228
# mkdir -p tmp
# Get transcript from Training data
out_dir=data/train
mkdir $out_dir 
# vocab=${out_dir}/All.vocab
corpus=data/text.forLMTrain
echo "Step 02.03: Build Language model for training"
# if [ ! -f $out_dir/lm_test.arpa.gz ]; then
	ngram-count -order 3 -unk -ndiscount -interpolate -limit-vocab -text "${corpus}" -lm $out_dir/lm_train.arpa.gz  -write-vocab  $out_dir/vocab 
# fi
echo "Step 02: Done!!!"
