#!/bin/bash
. ./path.sh
. ./cmd.sh
. ./configs.sh
stage=1
inDictLM=../G2P-STT
Training_lm=data/train/lm_train.arpa.gz
# Testing_lm=/home/thinhnt/workspace/kaldi/egs/vin/data/test/local/lm_test.arpa.gz

train_dict_name=Training-dict-OnlyBac-XSAMPA
# test_dict_name=Testing-dict-OnlyBac-XSAMPA

#############################
if [ $stage -le 1 ]; then	
	rm -fr data/local/${train_dict_name}
	cp ${inDictLM}/Training.lex Training.lex
	# echo "<noise> +noise+" >> Training.lex
	echo "ah +ah+" >> Training.lex
	echo "uhm +uhm+" >> Training.lex
	echo "oh +oh+" >> Training.lex
	echo "wow +wow+" >> Training.lex
	echo "<unk> +unk+" >> Training.lex
	cat Training.lex | sort | uniq > tmp.txt
	mv tmp.txt Training.lex
	dict=Training.lex
	echo "Step 03.01 Preparing Lexicon for training"
	local/fptasr_prepare_dict.sh ${dict} ${train_dict_name} || exit 1;
	cat data/local/${train_dict_name}/nonsilence_phones.txt \
		| grep -w -v "+ah+" \
		| grep -w -v "+uhm+" \
		| grep -w -v "+oh+" \
		| grep -w -v "+wow+"  > tmp.txt
	mv tmp.txt data/local/${train_dict_name}/nonsilence_phones.txt
	echo "+ah+" >> data/local/${train_dict_name}/silence_phones.txt
	echo "+uhm+" >> data/local/${train_dict_name}/silence_phones.txt
	echo "+oh+" >> data/local/${train_dict_name}/silence_phones.txt
	echo "+wow+" >> data/local/${train_dict_name}/silence_phones.txt
	# echo "+noise+" >> data/local/${train_dict_name}/silence_phones.txt
fi
#############################
# if [ $stage -le 2 ]; then	
# 	rm -fr data/local/${test_dict_name}
# 	cp ${inDictLM}/test/local/test.lex Testing.lex
# 	echo "<noise> +noise+" >> Testing.lex
# 	echo "ah +ah+" >> Testing.lex
# 	echo "uhm +uhm+" >> Testing.lex
# 	echo "wow +wow+" >> Testing.lex
# 	echo "<unk> +unk+" >> Testing.lex
# 	cat Testing.lex | sort | uniq > tmp.txt
# 	mv tmp.txt Testing.lex
# 	dict=Testing.lex
# 	echo "Step 03.02 Preparing Lexicon for testing"
# 	local/fptasr_prepare_dict.sh ${dict} ${test_dict_name} || exit 1;
# 	cat data/local/${test_dict_name}/nonsilence_phones.txt \
# 		| grep -w -v "+ah+" \
# 		| grep -w -v "+uhm+" \
# 		| grep -w -v "+oh+" \
# 		| grep -w -v "+wow+" \
# 		| grep -w -v "+noise+" > tmp.txt
# 	mv tmp.txt data/local/${test_dict_name}/nonsilence_phones.txt
# 	echo "+ah+" >> data/local/${test_dict_name}/silence_phones.txt
# 	echo "+uhm+" >> data/local/${test_dict_name}/silence_phones.txt
# 	echo "+oh+" >> data/local/${test_dict_name}/silence_phones.txt
# 	echo "+wow+" >> data/local/${test_dict_name}/silence_phones.txt
# 	echo "+noise+" >>  data/local/${test_dict_name}/silence_phones.txt
# fi

################# Prepare Lang for traning
if [ $stage -le 3 ]; then
	langout=data/lang_${train_dict_name}
	langwithlmout=data/lang_nosp_withLM_training_${train_dict_name}
	rm -rf ${langout}
	echo "Step 03.03: Preparing Lang for traning using Lexicon"
	rm -rf ${langout}
	utils/prepare_lang.sh data/local/${train_dict_name} "<unk>" data/local/lang_${train_dict_name} ${langout} || exit 1 ;
	rm -rf ${langwithlmout}
	echo "Step 03.04: Preparing Lang for traning using Language model"
	#local/arpa2G.sh $Training_lm data/lang_${outdict} data/lang_${outdict} || exit 1;
	srilm_opts="-subset -prune-lowprobs -order 3"
	utils/format_lm_sri.sh --srilm-opts "$srilm_opts" ${langout} $Training_lm data/local/${train_dict_name}/lexicon.txt ${langwithlmout}
fi

################# Prepare Lang for testing
# if [ $stage -le 4 ]; then
# 	langout=data/lang_${test_dict_name}
# 	refelang=data/lang_nosp_withLM_training_${train_dict_name}
# 	langwithlmout=data/lang_nosp_withLM_testing_${test_dict_name}
# 	rm -rf ${langout}
# 	echo "Step 03.05: Preparing Lang for testing using Lexicon"
# 	utils/prepare_lang.sh --phone-symbol-table ${refelang}/phones.txt \
# 		data/local/${test_dict_name} "<unk>" data/local/lang_${test_dict_name} ${langout} || exit 1 ;
# 	rm -rf ${langwithlmout}
# 	echo "Step 03.06: Preparing Lang for testing using Language model"
# 	#local/arpa2G.sh $Training_lm data/lang_${outdict} data/lang_${outdict} || exit 1;
# 	srilm_opts="-subset -prune-lowprobs -order 3"
# 	utils/format_lm_sri.sh --srilm-opts "$srilm_opts" ${langout} $Testing_lm data/local/${test_dict_name}/lexicon.txt ${langwithlmout}
# fi
# echo "Step 03: Done!!!"
