#!/bin/bash
################################################
# Necessary files: conf/mfcc.conf 
# Produced By Xinhui Hu, 2014/06/13
# Modified by Nguyen Van Huy, 2014/09/18
###############################################
. ./cmd.sh
. ./path.sh
. ./configs.sh

mfcc_config=conf/mfcc.conf
pitch_config=conf/pitch.conf
nj=4
echo "Step 05: Computing MFCC-Pitch feature using config from $mfcc_config $pitch_config"
for data in database_recording_raw_16k_1channel
do
	outdata=data/mfcc_pitch/${data}
	#rm -rf ${outdata}
	#mkdir -p ${outdata}
    	#cp -rf  ${data}/* ${outdata}/ || exit 1;
	utils/fix_data_dir.sh ${outdata} || exit 1;
	steps/make_mfcc_pitch.sh \
		--mfcc-config ${mfcc_config} \
		--cmd "$train_cmd" \
		--nj $nj \
		--pitch-config ${pitch_config} \
		${outdata} || exit 1;
    	utils/fix_data_dir.sh ${outdata} || exit 1;
    	steps/compute_cmvn_stats.sh ${outdata} || exit 1;
done
echo "Step 05: Done!!!"	

