#!/bin/bash
#########################################################################
# Training Kaldi GMMs using MFCC(13)+pitch(3) feature for Vietnam speech 
# Modified by Nguyen Van Huy, 2014/09/18
#
#########################################################################

. ./cmd.sh
. ./path.sh
. ./conf/common_vars.sh || exit 1;

stage=0
type=mfcc_pitch
# training acoustic models
TRAIN_mono=data/mfcc_pitch/mono
TRAIN_tri1=data/mfcc_pitch/database_recording_raw_16k_1channel
TRAIN_tri2=data/mfcc_pitch/database_recording_raw_16k_1channel
TRAIN_tri3=data/mfcc_pitch/database_recording_raw_16k_1channel
TRAIN_FULL=data/mfcc_pitch/database_recording_raw_16k_1channel

LEX=data/lang_nosp_withLM_training_Training-dict-OnlyBac-XSAMPA 


train_opt="--nj $njob --boost-silence $boost_sil --cmd $train_cmd"
train_opt1="--boost-silence $boost_sil --cmd $train_cmd"

if [ ${stage} -le 0 ]; then
  echo --- mono --- 
  echo "Step 06: Starting monophone training in exp${type}/mono" `date`
  echo ---------------------------------------------------------------------
      ./utils/subset_data_dir.sh $TRAIN_FULL 200000 $TRAIN_mono || exit 1;
      steps/train_mono.sh $train_opt $TRAIN_mono $LEX exp${type}/mono || exit 1;    
      utils/mkgraph.sh ${LEX} exp${type}/mono exp${type}/mono/graph
fi

if [ ${stage} -le 1 ]; then
  echo ---------------------------------------------------------------------- 
  echo "Step 06: triphone training in exp${type}/tri1 on" `date`
  echo ---------------------------------------------------------------------
      steps/align_si.sh $train_opt $TRAIN_tri1 $LEX exp${type}/mono exp${type}/mono_ali || exit 1;
      echo "numGaussTri1=" $numGaussTri1 "numLeavesTri1=" $numLeavesTri1
      steps/train_deltas.sh $train_opt1 $numLeavesTri1 $numGaussTri1 $TRAIN_tri1 $LEX exp${type}/mono_ali exp${type}/tri1 || exit 1;

      utils/mkgraph.sh ${LEX} exp${type}/tri1 exp${type}/tri1/graph
fi

if [ ${stage} -le 2 ]; then
  echo ---------------------------------------------------------------------
  echo "Step 06: triphone training in exp${type}/tri2 on" `date`
  echo ---------------------------------------------------------------------
    steps/align_si.sh $train_opt $TRAIN_tri2 $LEX exp${type}/tri1 exp${type}/tri1_ali || exit 1;
    steps/train_deltas.sh $train_opt1 $numLeavesTri2 $numGaussTri2 $TRAIN_tri2 $LEX exp${type}/tri1_ali exp${type}/tri2 || exit 1;
    utils/mkgraph.sh ${LEX} exp${type}/tri2 exp${type}/tri2/graph
fi

if [ ${stage} -le 3 ]; then
  echo ---------------------------------------------------------------------
  echo "Starting triphone training in exp${type}/tri3 on" `date`
  echo ---------------------------------------------------------------------

    steps/align_si.sh $train_opt $TRAIN_tri3 $LEX exp${type}/tri2 exp${type}/tri2_ali || exit 1;
    steps/train_deltas.sh $train_opt1 $numLeavesTri3 $numGaussTri3 $TRAIN_tri3 $LEX exp${type}/tri2_ali exp${type}/tri3 || exit 1;
    utils/mkgraph.sh ${LEX} exp${type}/tri3 exp${type}/tri3/graph
fi

if [ ${stage} -le 4 ]; then
  echo ---------------------------------------------------------------------
  echo "Starting (lda_mllt) triphone training in exp${type}/tri4 on" `date`
  echo ---------------------------------------------------------------------

    steps/align_si.sh $train_opt $TRAIN_FULL $LEX exp${type}/tri3 exp${type}/tri3_ali || exit 1; 
    steps/train_lda_mllt.sh $train_opt1 $numLeavesMLLT $numGaussMLLT $TRAIN_FULL $LEX exp${type}/tri3_ali exp${type}/tri4 || exit 1;
    utils/mkgraph.sh ${LEX} exp${type}/tri4 exp${type}/tri4/graph
fi

if [ ${stage} -le 5 ]; then
  echo ---------------------------------------------------------------------
  echo "Starting (SAT) triphone training in exp${type}/tri5a on" `date`
  echo ---------------------------------------------------------------------

    steps/align_si.sh $train_opt $TRAIN_FULL $LEX exp${type}/tri4 exp${type}/tri4_ali || exit 1;
    steps/train_sat.sh $train_opt1 $numLeavesSATa $numGaussSATa $TRAIN_FULL $LEX exp${type}/tri4_ali exp${type}/tri5a || exit 1;
    utils/mkgraph.sh ${LEX} exp${type}/tri5a exp${type}/tri5a/graph
fi

# if [ ${stage} -le 6 ]; then
#   echo ---------------------------------------------------------------------
#   echo "Starting align and make lat for exp${type}/tri5a on" `date`
#   echo ---------------------------------------------------------------------

#     steps/align_fmllr.sh $train_opt $TRAIN_FULL $LEX exp${type}/tri5a exp${type}/tri5a_ali || exit 1;
#     steps/align_fmllr_lats.sh --nj $nj --cmd "$train_cmd" $TRAIN_FULL \
#      $LEX exp${type}/tri5a exp${type}/tri5a_lat
# fi

echo "Done!!!!!!"



