#!/bin/bash
. ./cmd.sh
. ./path.sh
. ./configs.sh
type="mfcc_pitch"
data="data/mfcc_pitch/database_recording_raw_16k_1channel" 
gmm_dir="expmfcc_pitch/tri5a"
langin="lang_nosp_withLM_training_Training-dict-OnlyBac-XSAMPA"
echo "Step 07-cleaning: Cleaning database"
	cd data
	rm -f lang
	ln -s ${langin} lang
	cd -
	./local/run_cleanup_segmentation.sh ${data} ${gmm_dir}  || exit 1;
	mv ${data}_cleaned ${data}_cleaned_tmp || exit 1;
	mkdir -p ${data}_cleaned || exit 1;
	copy-feats scp:${data}_cleaned_tmp/feats.scp ark,scp:${data}_cleaned/feats.ark,${data}_cleaned/feats.scp || exit 1;
	cp -rf ${data}_cleaned_tmp/{spk2utt,text,wav.scp,utt2spk,segments} ${data}_cleaned/ || exit 1;
	./utils/fix_data_dir.sh ${data}_cleaned/ || exit 1;
	steps/compute_cmvn_stats.sh ${data}_cleaned ${data}_cleaned/log ${data}_cleaned/data || exit 1;
echo "Step 07-cleaning: Done!!!"
