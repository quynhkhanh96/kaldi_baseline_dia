#!/bin/bash
. ./cmd.sh
. ./path.sh
# PARAMETTERS
DNN_dir=expDNN #
train_set=data/mfcc_pitch/vin_2k5_argumented    # MFCC-Hires data (Argumented data)
oneof=200 # taking only 1/oneof cho UBM and PCA training
gmm_gauss=1024 # Number of Gaussian mixtures for UBM model (512 for small set)

### ACTUAL EXECUTATION
echo "Step 07: Computing i-Vector for TDNN training"
./local/nnet3/run_ivector_common_pca_vt.sh ${DNN_dir} ${train_set} ${oneof} ${gmm_gauss} || exit 1;
echo "Step 07: Done!!!"