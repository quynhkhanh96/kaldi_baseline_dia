#!/bin/bash
. ./path.sh
. ./cmd.sh
. ./configs.sh

cuda_devices="0,1,2,3" # list of Cuda devices for training model, determined by ','
###### Paths
dir=expDNN/cnn_tdnnf_80job # Working DNN dir for this setup
langdnn=data/lang_dnn # DNN lang topo (1 stage HMM)
langtrain=data/lang_nosp_withLM_training_Training-dict-OnlyBac-XSAMPA # Referrence Lang for DNN training, should be the HMM-GMM lang training. 
train_set_for_tree=data/mfcc_pitch/vin_2k5 #Data used that associates with the input Alignments
gmm_dir=expmfcc_pitch/tri5a # GMM-HMM model dir for tree tranining
ali_dir=expmfcc_pitch/tri5a_ali # Alignment dir for tree training
lat_dir=expmfcc_pitch/tri5a_lats_augmented_03 # Lattices dir that have to associate with train_data_dir (MFCC-Hires)
train_data_dir=data/mfcc_pitch/vin_2k5_argumented # The input data for DNN training, 
                                                        # should be argumented data in kind of MFCC-Hires feature
train_ivector_dir=expDNN/ivectors_vin_2k5_argumented #The i-vector data that associate with train_data_dir (MFCC-Hires), 
                                    # and extracted from script 10.Extract_ivector_pca
extractor=expDNN/extractor # The i-vector extractor

### Using a pre-trained model
trained_model="None" # pre-trained model that have to be the same topo, set to None to skip it
                     #  If specified, this model is used as initial raw model (0.raw in the script) instead of
                     #  initializing the model from the xconfig. Also configs dir is not expected to exist and 
                     #  left/right context is computed from this model

##### Stages
train_stage=-10 # DNN training stage (begining at -10)
    # -6 for Creating phone language-model
    # -5 Creating denominator FST
    # -4 Initializing a basic network for estimating preconditioning matrix
    # -3 Generating egs
    # -2 Computing the preconditioning matrix for input features
    # -1 Preparing the initial acoustic model
    # 0 for the first interation (0)
    # N to start from iter N
get_egs_stage=-10 # egs extracting stage (begining at -10)
stage=3 # process stage: 1 for making lang dnn, 2 for building the tree, 3 for configing DNN topo, 4 for DNN training

#### Magical numbers
num_epoch=5 # Number of epoches for DNN training (should be more than 3)
init_num_jobs=2 # Number of paralell jobs at begining of DNN training 
final_num_jobs=8 # Number of paralell jobs at lats epoch of DNN training. Careful on GPU-Mem = final_num_jobs * size_of_one_job
minibatch=64 # minibatch sizes, like: 128,64
init_LrRate=0.0015 # initial learning rate, normal is 0.001
final_LrRate=0.00015 # final learning rate, normal is 0.0001
############# 
./local/chain/run_cnn_tdnn_vt_v0.sh \
    ${dir} \
    ${langdnn} \
    ${langtrain} \
    ${train_set_for_tree} \
    ${gmm_dir} \
    ${ali_dir} \
    ${lat_dir} \
    ${train_data_dir} \
    ${train_ivector_dir} \
    ${extractor} \
    ${train_stage} \
    ${get_egs_stage} \
    ${stage} \
    ${num_epoch} \
    ${init_num_jobs} \
    ${final_num_jobs} \
    ${minibatch} \
    ${init_LrRate} \
    ${final_LrRate} \
    ${trained_model} \
    ${cuda_devices} \
    || exit 1;
echo "Step 08: Done!!!"
