#keyword search default
glmFile=conf/glm
duptime=0.5
case_insensitive=false
cer=0
# Acoustic model parameters
numLeavesTri1=2000
numGaussTri1=16000
numLeavesTri2=2000
numGaussTri2=16000
numLeavesTri3=3000
numGaussTri3=60000
numLeavesMLLT=5000
numGaussMLLT=100000
numLeavesSATa=5000
numGaussSATa=100000
numLeavesSATb=5000
numGaussSATb=100000
# Lexicon and Language Model parameters
oovSymbol="<unk>"
lexiconFlags="--oov <unk>"
use_pitch=false
#########
boost_sil=1.5

