#!/bin/bash
. ./cmd.sh
. ./path.sh
. ./conf/common_vars.sh || exit 1;
type=mfcc_pitch
cmd=run.pl
testdata=data/mfcc_pitch/test-6k

local/score.sh --cmd "$cmd" --decode_mbr "true" \
	${testdata} \
	exp${type}/tri4_mmi/graph_pocolm_onlyTraninngTrans \
	exp${type}/tri4_mmi/decode_graph_pocolm_onlyTraninngTrans
